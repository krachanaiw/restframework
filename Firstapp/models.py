from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Profile(models.Model):
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=10)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.address