from django.shortcuts import render

# Create your views here.
from Firstapp.models import Profile
from rest_framework import viewsets

from Firstapp.serializer import ProfileReadSerializer, ProfileWriteSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset=Profile.objects.all()
    serializer_class = ProfileReadSerializer

    # def get_queryset(self):#filter according to user_id
        # qs= Profile.objects.filter(user_id=3)
        # return  qs

    def get_serializer_class(self): #post
        if self.request.POST:
            self.serializer_class= ProfileWriteSerializer
            return self.serializer_class
        else:
            return ProfileReadSerializer

    # def create(self, request, *args, **kwargs):
