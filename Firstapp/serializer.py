from django.contrib.auth.models import User
from rest_framework import serializers

from Firstapp.models import Profile


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class ProfileReadSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = Profile
        fields = ['id','user', 'address', 'phone']


class ProfileWriteSerializer(serializers.ModelSerializer): #it makes post possible
    class Meta:
        model=Profile
        fields='__all__'
