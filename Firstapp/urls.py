from django.urls import include, path
from rest_framework import routers
from Firstapp import views

router = routers.DefaultRouter()
router.register(r'profile', views.ProfileViewSet)
urlpatterns = [
   path('api', include(router.urls)),

]